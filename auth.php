<?php

    require_once 'controller.php';
    $mainCtrl = new Controller();
    $adminCheckAuth = $mainCtrl->hasUserAuthorized();
    $errorAuth = 0;
    if($adminCheckAuth < 0) {
        $errorAuth = 1;
        $adminCheckAuth = 0;
    }

   /* if($adminCheckAuth){
        header('Location: ./');
    }*/
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
              crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
                integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
                crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
                crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
                integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
                crossorigin="anonymous">
        </script>

        <script type="text/javascript">
            $(document).ready(function(){

                <?php if($errorAuth):  ?>
                    alert('Имя пользователя или пароль указаны неверно.');
                <?php endif;?>
                $('#listTask').click(function () {
                    location.href='./';
                });

            });
        </script>
    </head>
    <body>

        <div class="container" style="padding-left: 50px;padding-top: 50px">
           <div class="row">
            <div class="col-sm-12">
               <div class="row" >
                   <div class="row alert alert-secondary" role="alert" style="width: 100%">
                      <div class="col-sm-4">Авторизация</div>
                       <div class="col-sm-8" style="text-align: right">
                           <button type="button" class="btn btn-primary" name="listTask" id="listTask">Список задач</button>
                       </div>

                   </div>
                   <form class="needs-validation" action="# " method="post">
                       <div class="form-group" >
                           <label for="userName">User name</label>
                           <input type="text"
                                  class="form-control"
                                  id="userName"
                                  name="userName"
                               <?php if($adminCheckAuth){print('disabled');}?>
                                  required>
                           <div class="invalid-feedback">
                               Please choose a username.
                           </div>
                       </div>


                       <div class="form-group">
                           <label for="userPassword">Password</label>
                           <input type="password"
                                  class="form-control"
                                  id="userPassword"
                                  name="userPassword"
                               <?php if($adminCheckAuth){print('disabled');}?>
                                  required>
                           <div class="invalid-feedback">
                               Please choose a password.
                           </div>
                       </div>

                       <input type="hidden" name="checkAuth" value="<?php echo $adminCheckAuth; ?>">

                       <button type="submit" class="btn btn-primary" name="login">
                           <?php print ( $adminCheckAuth  ? 'Выход' : 'Войти' ); ?>
                       </button>

                   </form>
                   <?php

                   ?>
               </div>
           </div>
        </div>

        </div>
    </body>
</html>
