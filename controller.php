<?php
    require_once 'TasksDb.php';



    class Controller {

        public $taskDb;
        public $listTask=[];
        public $pageCount;

        function __construct()
        {
        }

        function hasUserAuthorized(){
            if(isset($_POST["login"])){
                if(isset($_POST["checkAuth"]) && $_POST["checkAuth"]){
                    unset($_POST["login"]);
                    return 0;
                }

                $userName = $_POST["userName"];
                $userPsw = $_POST["userPassword"];

                if((strcmp($userName, 'admin')!== 0) || (strcmp($userPsw, '123')!== 0) ){
                    setcookie('adminCheckAuth',0);
                    setcookie('errorAuth',1);
                    return -1;
                }

                setcookie('adminCheckAuth',1);
                setcookie('errorAuth',0 );
                return 1;
            }


            setcookie('adminCheckAuth',0);
            setcookie('errorAuth',0 );

            return 0;
        }

        function addTask(){
            $this->taskDb = new TasksDb();
            $dataFields[0] = htmlspecialchars(strip_tags($_POST["userNameTask"]));
            $dataFields[1] = htmlspecialchars(strip_tags($_POST["userMailTask"]));
            $dataFields[2] = htmlspecialchars(strip_tags($_POST["textTask"]));
            $dataFields[3] = isset($_POST["statusTask"]) ? $_POST["statusTask"] : 0;
            $this->taskDb->insertTask($dataFields);
            unset($this->taskDb);
        }

        function updateTask(){
            $this->taskDb = new TasksDb();
            $dataFields[0] = htmlspecialchars(strip_tags($_POST["textTask"]));
            $dataFields[1] = isset($_POST["statusTask"]) ? $_POST["statusTask"] : 0;
            $dataFields[2] = isset($_POST["modifyTaskText"]) ? $_POST["modifyTaskText"] : 0;
            $dataFields[3] =  htmlspecialchars(strip_tags($_POST["idTask"]));
            $this->taskDb->updateTask($dataFields);
            unset($this->taskDb);
        }

        function getListTask($withSort, $sortField){
            $this->taskDb = new TasksDb();
            $this->listTask = $this->taskDb->getData($withSort, $sortField);
            $this->pageCount = count($this->listTask)%3 !=0 ? ((int)(count($this->listTask)/3) +1): ((int)(count($this->listTask)/3));
            unset($this->taskDb);
        }

        function findTask($id){
            $this->taskDb = new TasksDb();
            $this->findTask($id);
            unset($this->taskDb);
        }
    }

?>