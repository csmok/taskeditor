<?php

require_once 'controller.php';

$mainCtrl = new Controller();
$adminCheckAuth = isset($_COOKIE["adminCheckAuth"]) ? $_COOKIE["adminCheckAuth"] : 0;
if(isset($_GET["fields"]) && isset($_GET["sort"])){
    if($_GET["sort"] > 0){
        $mainCtrl->getListTask($_GET["sort"] == 1 ? 'asc' : 'desc', $_GET["fields"] );
    } else {
        $mainCtrl->getListTask(null ,null);
    }

} else {
    $mainCtrl->getListTask(null ,null);
}

 $currentPage = isset($_COOKIE["currentPage"]) ? $_COOKIE["currentPage"] : 1;
 if(isset($_POST["firstPage"])){ $currentPage = 1;}
 if(isset($_POST["previusPage"])){ $currentPage --;}
 if(isset($_POST["nextPage"])){ $currentPage ++;}
 if(isset($_POST["lastPage"])){ $currentPage = $mainCtrl->pageCount;}
 if($currentPage<=0){$currentPage = 1;}
if($currentPage>$mainCtrl->pageCount){$currentPage = $mainCtrl->pageCount;}

 setcookie('currentPage',$currentPage);

?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
              crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
                integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
                crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
                crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
                integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
                crossorigin="anonymous">
        </script>

        <script type="text/javascript">
            $(document).ready(function(){
               var adminCheckAuth = <?php print($adminCheckAuth);?> ;
               var sortFields = "<?php print(isset($_GET['fields']) ? $_GET['fields']:'');?>";
               var sortType = <?php print(isset($_GET['sort']) ? $_GET['sort']:0);?>;

               <?php if(isset($_GET['addTask'])): ?>
                    alert('Добавлена новая задача.');
                    location.href = './';
                <?php endif; ?>

               if(sortFields == 'task_username'){
                   $("#sortName").val(sortType);
               }

                if(sortFields == 'task_usermail'){
                    $("#sortMail").val(sortType);
                }


                $("#loginUser").click(function(){

                    if (adminCheckAuth) {
                        document.cookie = "adminCheckAuth=0";
                        location.href = './';
                    } else {
                        location.href = './auth.php';
                    }

                });

                $("#addTask").click(function(){
                    location.href = './addTask.php';
                });

                $("#sortName").click(function(){
                    var sortFlag = parseInt($("#sortName").val());
                    $("#sortMail").val(0);
                    sortFlag = (sortFlag === 2) ? 0 : sortFlag+1;
                    $("#sortName").val(sortFlag);
                    var hrefStr ='./';
                    if(sortFlag > 0){
                       hrefStr = './index.php?fields=task_username&sort='+sortFlag;
                    }

                    location.href = hrefStr;
                });

                $("#sortMail").click(function(){
                    var sortFlag = parseInt($("#sortMail").val());
                    $("#sortName").val(0);
                    sortFlag = (sortFlag === 2) ? 0 : sortFlag+1;
                    $("#sortMail").val(sortFlag);
                    var hrefStr ='./';
                    console.log(sortFlag);
                    if(sortFlag > 0){
                        hrefStr = './index.php?fields=task_usermail&sort='+sortFlag;
                    }

                    location.href = hrefStr;
                });

            });
        </script>
    </head>
    <body>

        <div class="container" style="padding-left: 50px;padding-top: 50px">

           <div class="row alert alert-dark">
                    <div class="col-sm-4"> Список задач </div>
                    <div class="col-sm-8" style="text-align: right">
                        <button type="button" class="btn btn-primary" name="addTask" id="addTask">Добавить задачу</button>
                        <button type="button" class="btn btn-primary" name="loginUser" id="loginUser" >
                            <?php print ( $adminCheckAuth  ? 'Выход': 'Авторизация'); ?>
                        </button>
                    </div>
        </div>
            <div class="row">
                <table class="table">
                <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">
                         <button type="button" class="btn btn-primary" id="sortName" name="sortName" value="0" style="width: 100%"> Имя пользователя </button>
                    </th>
                    <th scope="col">
                        <button type="button" class="btn btn-primary" id="sortMail" name="sortMail" value="0" style="width: 100%">E-mail</button></th>
                    <th scope="col">
                        <button type="button" class="btn btn-primary" name="sortTask" value="0" style="width: 100%">Текст задачи</button>
                    </th>
                    <th scope="col">
                        <button type="button" class="btn btn-primary" name="sortStatus" value="0" style="width: 100%">Отметка</button>
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php for($i=0; $i<3;$i++):?>
                <?php if(isset($mainCtrl->listTask[$i+3*($currentPage-1)]['task_username'])):?>
                <tr>
                    <th scope="row">
                        <form action="addTask.php" method="post" <?php print($adminCheckAuth ? '' : 'hidden');?> >
                            <button type="submit" class="btn btn-primary" name="updateBtn_<?php print($i+1);?>">
                              Изменить
                            </button>
                            <input type="hidden" name="valueId_<?php print($i+1);?>" value="<?php print($i+3*($currentPage-1));?>">
                        </form>
                    </th>
                    <td id="fields_1"><?php print($mainCtrl->listTask[$i+3*($currentPage-1)]['task_username']); ?></td>
                    <td id="fields_2"><?php print($mainCtrl->listTask[$i+3*($currentPage-1)]['task_usermail']); ?></td>
                    <td id="fields_3"><?php print($mainCtrl->listTask[$i+3*($currentPage-1)]['task_text']); ?></td>
                    <td id="fields_4"><?php
                        $statusTask = '';
                        if(!!$mainCtrl->listTask[$i+3*($currentPage-1)]['task_admincheck']){$statusTask = 'выполнено <br>';}
                        if(!!$mainCtrl->listTask[$i+3*($currentPage-1)]['task_adminmodify'])
                        {$statusTask = $statusTask.'отредактировано администратором';}
                        print($statusTask);
                        ?></td>
                </tr>
                <?php endif; ?>
               <?php endfor;?>
                </tbody>
                </table>

            </div>
            <div class="row">
                <form action="#" method="post">
                    <button type="submit" class="btn btn-primary" name="firstPage"> << </button>
                    <button type="submit" class="btn btn-primary" name="previusPage"> < </button>
                    <button type="submit" class="btn btn-primary" name="nextPage"> > </button>
                    <button type="submit" class="btn btn-primary" name="lastPage"> >> </button>
                </form>

            </div>

        </div>
    </body>
</html>