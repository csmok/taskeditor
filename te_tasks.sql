SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


DROP TABLE IF EXISTS `te_tasks`;
CREATE TABLE `te_tasks` (
  `id` int(11) NOT NULL,
  `task_username` varchar(255) COLLATE utf8_bin NOT NULL,
  `task_usermail` varchar(255) COLLATE utf8_bin NOT NULL,
  `task_text` text COLLATE utf8_bin NOT NULL,
  `task_admincheck` tinyint(1) NOT NULL,
  `task_adminmodify` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `te_tasks` (`id`, `task_username`, `task_usermail`, `task_text`, `task_admincheck`, `task_adminmodify`) VALUES
(1, 'test', 'test@test.com', 'test job', 1, 1),
(2, 'test1', 'test1@test.com', 'alert(‘test’);', 0, 0),
(3, 'test2', 'test2@test.com', 'test2 job', 0, 0),
(4, 'test3', 'test3@test.com', 'test job', 0, 0);


ALTER TABLE `te_tasks`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `te_tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
