<?php
    class TasksDb
    {
        public $connectDb;



        function  __construct()
        {
           $hostDb = 'localhost';
           $loginDb = 'id12667041_taskeditor';
           $passwordDb = '@sxjDo@b7KNP2zu#qeJhRQkd';
           $nameDb = 'id12667041_taskeditor';

           /* $hostDb = 'localhost';
            $loginDb = 'root';
            $passwordDb = '';
            $nameDb = 'taskeditor';
*/
            $this->connectDb = new mysqli( $hostDb, $loginDb, $passwordDb, $nameDb);

        }

        function  insertTask($dataFields){
            $query = 'insert into te_tasks (task_username, task_usermail, task_text , task_admincheck) values (?, ?, ? , ?)';
            $stmt = $this->connectDb->prepare($query);
            $stmt->bind_param('sssi',$f1,$f2,$f3,$f4);
            $f1 = $dataFields[0];
            $f2 = $dataFields[1];
            $f3 = $dataFields[2];
            $f4 = $dataFields[3];
            $stmt->execute();
            $stmt->close();
            $this->connectDb->close();
        }

        function  updateTask($dataFields){
            $query = 'update te_tasks set  task_text = ? , task_admincheck = ?, task_adminmodify = ? '.
                    'where id = ?';
            $stmt = $this->connectDb->prepare($query);
            $stmt->bind_param('siii',$f1,$f2,$f3,$f4);
            $f1 = $dataFields[0];
            $f2 = $dataFields[1];
            $f3 = $dataFields[2];
            $f4 = $dataFields[3];
            $stmt->execute();
            $stmt->close();
            $this->connectDb->close();
        }

        function getData($withSort, $sortField){
            $query = 'select id,task_username, task_usermail, task_text , task_admincheck , task_adminmodify from te_tasks ';
            if(!!$withSort){
                $query = $query.'order by '.$sortField.' '.$withSort;
            }

            $result = $this->connectDb->query($query);
                $i = 0;
            $listTask = [];
                while ($row = $result->fetch_assoc()) {
                    $listTask[$i] ['id'] = $row['id'];
                    $listTask[$i] ['task_username'] = $row['task_username'];
                    $listTask[$i] ['task_usermail'] = $row['task_usermail'];
                    $listTask[$i] ['task_text'] = $row['task_text'];
                    $listTask[$i] ['task_admincheck'] = $row['task_admincheck'];
                    $listTask[$i] ['task_adminmodify'] = $row['task_adminmodify'];
                    $i++;
                }

                $result->close();

                $this->connectDb->close();
                return $listTask;

            }

    }
?>