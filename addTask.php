<?php

require_once 'controller.php';

$mainCtrl = new Controller();

if(isset($_POST["updateBtn_1"]) || isset($_POST["updateBtn_2"]) || isset($_POST["updateBtn_3"])){
    for($i=1;$i<4;$i++)
    {
        if(isset($_POST["updateBtn_".$i])){$updateData = $_POST["valueId_".$i]; }
    }

    $mainCtrl->getListTask(null ,null);
    $findTask[0] =  $mainCtrl->listTask[$updateData] ['id'];
    $findTask[1] =  $mainCtrl->listTask[$updateData] ['task_username'] ;
    $findTask[2] =  $mainCtrl->listTask[$updateData] ['task_usermail'];
    $findTask[3] =  $mainCtrl->listTask[$updateData] ['task_text'];
    $findTask[4] =  $mainCtrl->listTask[$updateData] ['task_admincheck'];
    $findTask[5] =  $mainCtrl->listTask[$updateData] ['task_adminmodify'];

} else {
    $updateData = -1;
}
$adminCheckAuth = isset($_COOKIE["adminCheckAuth"]) ? $_COOKIE["adminCheckAuth"] : 0;
if(isset($_POST["addTask"])){
    if(((int)$_POST["idTask"])<0){
        $mainCtrl->addTask();
        header('Location: ./index.php?addTask=1');
    } else{
        if ($adminCheckAuth){
            $mainCtrl->updateTask();
            header('Location: ./');
        } else {
            header('Location: ./auth.php');
        }

    }

}
?>

<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous">
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#textTask").val('');

            <?php if($updateData>=0):?>
                $("#idTask").val("<?php print ($findTask[0]);?>");
                $("#userNameTask").val("<?php print ($findTask[1]);?>");
                $("#userMailTask").val("<?php print ($findTask[2]);?>");
                $("#textTask").val("<?php print ($findTask[3]);?>");
                $("#statusTask").val("<?php print ($findTask[4]);?>");
                $("#statusTask").prop('checked',<?php print ($findTask[4]== 1 ? 'true' : 'false');?>);
            <?php endif;?>


            $("#loginUser").click(function(){
                location.href = './auth.php';
            });

            $("#listTask").click(function() {
                location.href = './';
            });

            $('#textTask').change(function(){
                $('#modifyTaskText').val(1);
              });

            $("#statusTask").change(function() {
               if($("#statusTask").prop('checked')) {
                   $("#statusTask").val(1);
               } else {
                   $("#statusTask").val(0);
               }
            });

        });
    </script>
</head>
<body>

<div class="container" style="padding-left: 50px;padding-top: 50px">
    <div class="row">
        <div class="col-sm-12">
            <div class="row" style="width: 100%">
                <div class="row alert alert-secondary" role="alert" style="width: 100%">
                    <div class="col-sm-4" >
                        Добавление задачи
                    </div>
                    <div class="col-sm-8" style="text-align: right">
                        <button type="button" class="btn btn-primary" name="listTask" id="listTask">Список задач</button>
                        <button type="button" class="btn btn-primary" name="loginUser" id="loginUser" >
                           <?php
                                print ( $adminCheckAuth  ? 'Выход' : 'Авторизация' );
                            ?>
                        </button>
                    </div>

                </div>
                <form class="needs-validation" action="# " method="post">
                    <div class="form-group" >
                        <label for="userNameTask">Имя пользователя</label>
                        <input type="text"
                               class="form-control"
                               id="userNameTask"
                               name="userNameTask"
                            <?php if($updateData>=0){print('disabled');}?>
                               required>
                        <div class="invalid-feedback">
                            Введите имя пользователя.
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="userMailTask">Адрес электронной почты</label>
                        <input type="email"
                               class="form-control"
                               id="userMailTask"
                               name="userMailTask"
                            <?php if($updateData>=0){print('disabled');}?>
                               required>
                        <div class="invalid-feedback">
                            Введите e-mail.
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="textTask">Задача</label>
                        <textarea
                            class="form-control"
                            id="textTask"
                            name="textTask"
                            required>
                        </textarea>
                               <div class="invalid-feedback">
                                   Введите текст задачи.
                               </div>
                    </div>

                    <?php if($adminCheckAuth):?>
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="statusTask" name="statusTask">
                                <label class="form-check-label" for="statusTask">
                                    Статус задачи
                                </label>
                            </div>
                        </div>
                    <?php endif;?>
                    <input type="hidden" name="idTask" id="idTask" value="-1">
                    <input type="hidden" name="modifyTaskText" id="modifyTaskText" value="0">
                    <button type="submit" class="btn btn-primary" name="addTask">Сохранить</button>

                </form>
            </div>
        </div>

    </div>

</div>
</body>
</html>